class Course{
  String author;
  String authorImg;
  String title;
  String imageUrl;

  Course(this.author, this.authorImg, this.title, this.imageUrl);

  static List<Course> generateCourse() {
    return [
      Course('Mahdi Andalib','assets/images/mahdi.jpg','flutter','assets/images/flutter-tutorial.jpg'),
      Course('Mohammad Zatkhahi','assets/images/mohammad.jpg','Laravel','assets/images/laravel-tutorial.png')
    ];
  }
}